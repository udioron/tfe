import xmlrpclib
from django.conf import settings


def get_client():
    host = settings.TF_SERVICE_HOST
    port = settings.TF_SERVICE_PORT
    client = xmlrpclib.ServerProxy('http://{}:{}'.format(host, port))
    return client

Setup:

    pip install django

Usage on linux:

Start XMLRPC service:

    export PYTHONPATH='/path/to/my/modules/root/'
    ./tf my_pkg.my_module.MyClass 9876

    ./manage.py runserver 0.0.0.0:8000

Usage on windows:

    set PYTHONPATH=c:\path\to\my\modules\root\
    tf my_pkg.my_module.MyClass 9876

    python manage.py runserver 0.0.0.0:8000

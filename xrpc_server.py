from SimpleXMLRPCServer import SimpleXMLRPCServer, SimpleXMLRPCDispatcher
import SocketServer
import inspect
import sys
import logging
import traceback
import xmlrpclib

logger = logging.getLogger(__name__)


def list_public_methods(obj):
    """Returns a list of attribute strings, found in the specified
    object, which represent callable attributes"""

    return [member for member in dir(obj)
            if not member.startswith('_') and
               hasattr(getattr(obj, member), '__call__')]


class CustomXMLRPCServer(SocketServer.ThreadingMixIn, SimpleXMLRPCServer):
    def __init__(self, instance, *args, **kwargs):
        kw = kwargs.copy()
        if 'allow_none' not in kw:
            kw['allow_none'] = True
        SimpleXMLRPCServer.__init__(self, *args, **kw)
        self.register_instance(instance)
        self.register_function(self._introspect_instance, "system.introspect")
        self.register_function(self._shutdown, "system.shutdown")


    def _dispatch(self, method, params):
        logger.info("CALL {}({})".format(method, ", ".join(
            [repr(p) for p in params])))
        try:
            return SimpleXMLRPCDispatcher._dispatch(self, method, params)
        except Exception:
            exc_type, exc_value, exc_tb = sys.exc_info()
            tb = traceback.format_exc()
            msg = "%s: %s\n%s" % (exc_type, exc_value, tb)
            logger.exception("FAULT {}: {}".format(method, msg))
            raise xmlrpclib.Fault(1, msg)

    def _shutdown(self):
        logger.info("Shutdown requested...")
        self.shutdown()
        return True


    def _get_method_list(self):
        assert self.instance, "Instance must be registered"
        # Instance can implement _listMethod to return a list of
        # methods
        if hasattr(self.instance, '_listMethods'):
            methods = self.instance._listMethods()

        # if the instance has a _dispatch method then we
        # don't have enough information to provide a list
        # of methods
        else:
            assert not hasattr(self.instance, '_dispatch'), \
                "Instances which implement _dispatch are not supported"
            methods = list_public_methods(self.instance)

        return sorted(methods)

    def _get_method_params(self, method):
        return inspect.getargspec(getattr(self.instance, method)).args[1:]

    def _get_method_help(self, method_name):
        method = getattr(self.instance, method_name)
        import pydoc

        return pydoc.getdoc(method)

    def _introspect_method(self, method):
        return {
            'args': self._get_method_params(method),
            'doc': self._get_method_help(method),
        }

    def _introspect_instance(self):
        import pydoc

        return {
            'name': self.instance.__class__.__name__,
            'doc': pydoc.getdoc(self.instance),
            'methods': {m: self._introspect_method(m) for m in
                        self._get_method_list()},
        }



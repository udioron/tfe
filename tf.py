import sys
import os.path
import logging
from xrpc_server import CustomXMLRPCServer

logger = logging.getLogger(__name__)


def import_class(cl):
    d = cl.rfind(".")
    classname = cl[d + 1:len(cl)]
    m = __import__(cl[0:d], globals(), locals(), [classname])
    return getattr(m, classname)


def start_service(cls, port):
    logger.info("Listening on port {}, {}".format(port, cls.__name__))
    instance = cls()
    server = CustomXMLRPCServer(instance, ("0.0.0.0", port), allow_none=True)
    server.serve_forever()
    logger.info("... shutdown completed.")


if __name__ == '__main__':
    if len(sys.argv) != 3:
        raise Exception(
            "USAGE: {} CLASS PORT".format(os.path.basename(sys.argv[0])))

    logging.basicConfig(
        level=logging.INFO,
        format="[%(levelname)1.1s %(asctime)s %(module)s:%(lineno)d] %(message)s")

    cls_name = sys.argv[1]
    port = int(sys.argv[2])
    cls = import_class(cls_name)
    start_service(cls, port)

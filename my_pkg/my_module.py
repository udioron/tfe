import logging
import time

logger = logging.getLogger(__name__)

#f = open('udi.txt', 'w')


class MyClass(object):
    """ this is the module doc
    """

    def my_method(self):
        """
        cool
        """
        print "my_method called"
        time.sleep(5)
        return "OK", [1, 2, 3], 4.5, {"status": "running"}

    def bad_method(self, x):
        return 10 / 0

    def add(self, a, b):
        return int(a) + int(b)

    def makelist(self, a, b, c):
        return [a, b, c]

    def get_none1(self):
        pass

    def get_none2(self):
        return

    def get_none3(self):
        return None

    def with_defaults(self, a, b="2", c=3):
        return [a, b, c]


    def buggy(self):
        """Hello World!

        This is the functions' doc.
        Enjoy!
        """

        def bar():
            assert False, "Hello World"

        def foo():
            bar()

        foo()

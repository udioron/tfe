@echo off
:start
python tf.py %*
if errorlevel 1 goto end
echo ============================================
echo Restarting...
echo ============================================
goto start

:end
echo Aborted (Exit code: %ERRORLEVEL%)
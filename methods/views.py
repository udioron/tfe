from xmlrpclib import Fault

from django.http import HttpResponse
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import ensure_csrf_cookie
from django.views.generic import TemplateView, View
from django.views.generic.base import TemplateResponseMixin, ContextMixin

from xrpc_api import get_client


class ListMethodsView(TemplateView):
    template_name = "methods/method_list.html"

    @method_decorator(ensure_csrf_cookie)
    def dispatch(self, request, *args, **kwargs):
        return super(ListMethodsView, self).dispatch(request, *args, **kwargs)


    def get_context_data(self, **kwargs):
        client = get_client()
        d = super(ListMethodsView, self).get_context_data(**kwargs)
        d['info'] = client.system.introspect()
        return d


class MethodDetailView(TemplateResponseMixin, ContextMixin, View):
    template_name = "methods/method_detail.html"

    def post(self, request, *args, **kwargs):
        values = request.POST.getlist('params')
        client = get_client()
        method = self.request.GET['method']
        d = self.get_context_data(values=values, method=method)
        try:
            d['result'] = getattr(client, method)(*values)
        except Fault as e:
            d['error'] = e
        return render(request, 'methods/method_execute.html', d)


class RestartRPCServerView(View):
    def post(self, request, *args, **kwargs):
        client = get_client()
        client.system.shutdown()
        return HttpResponse("OK")

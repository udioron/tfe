$(function () {
    $('form').submit(function () {
        var form = $(this);
        $.post(form.attr('action'), form.serialize(),
            function (data) {
                $('button').prop('disabled', false);
                $("#results").prepend($(data.trim()));
            }
        );
        $('button').prop('disabled', true);
        return false;
    });

    $('.restart').click(function () {
        var btn = $(this);
        $.post(btn.data('href'), {},
            function (data) {
                $('button').prop('disabled', false);
                var el = $('<span class="label label-success">Service Restarted</span>');
                btn.before(el);
                el.fadeOut(1000);
            }
        );
        $('button').prop('disabled', true);
        return false;
    });
});

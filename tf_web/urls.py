from django.conf.urls import patterns, include, url

from django.contrib import admin
from methods import views

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', views.ListMethodsView.as_view(), name='home'),
    url(r'^method/$', views.MethodDetailView.as_view(), name='method'),
    url(r'^restart/$', views.RestartRPCServerView.as_view(), name='restart'),

    url(r'^admin/', include(admin.site.urls)),
)

#!/bin/bash

# break on any error:
set -e

while true; do
    python tf.py $*
    echo "======================="
    echo "Restarting"
    echo "======================="
done

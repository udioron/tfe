import inspect


class Bar(object):
    def f(x, y, z):
        pass


def get_params(cls, f):
    return inspect.getargspec(getattr(cls, f)).args


print get_params(Bar, 'f')
assert get_params(Bar, 'f') == ['x','y','z']